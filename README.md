# Spreedly-Java #

This is a Java based API for Spreedly (https://docs.spreedly.com/).

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

### How to release a new version? ###

* Checking the software out: git clone https://bitbucket.org/niyamit/spreedly-java.git
* Giving it a version: $mvn versions:set
* Building, testing, packaging: $mvn source:jar javadoc:jar install;
* Move the local maven installed repository into project's internal maven repository: $mv ~/.m2/repository/com/niyamit/spreedly/…/version-xx PROJECTHOME/m2/repository/com/…./
* Tagging this state in SCM: $mvn scm:tag
* Commit and push: $git commit; git push
