package com.niyamit.spreedly;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author bsneade
 */
public class SpreedlyGatewayServiceIntegrationTest {
    
    private final SpreedlyGatewayService spreedlyGatewayService = new SpreedlyGatewayService();
    private final ObjectFactory objectFactory = new ObjectFactory();
    
    @BeforeClass
    public void beforeClass() {
        //configure the service
        spreedlyGatewayService.setEnvironmentKey("KPAdXK2qCu0NClgNmvfT45Q0jIu");
        spreedlyGatewayService.setAccessSecret("W3NcbvqpLFNv3ZV9m3Cbj9id4ygkRmQtjFc9J22tsYhnzxkKqjuEqINXBnAijpud");
        
        final Client client = ClientBuilder.newClient()
                .register(new LoggingInterceptor());
        
        spreedlyGatewayService.setClient(client);
    }
    
    @Test
    public void testAdd() {
        //set up data
        final Gateway gateway = objectFactory.createGateway();
        gateway.setGatewayType("test");

        //call method under test
        final Gateway returnGateway = spreedlyGatewayService.add(gateway);

        //assertions
    }
    
    @Test
    public void testRetain() {
        //TODO - requires manual form submittal first
    }
    
}
