package com.niyamit.spreedly;

import com.niyamit.spreedly.paymentmethods.CreditCard;
import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import com.niyamit.spreedly.paymentmethods.PaymentMethods;
import com.niyamit.spreedly.transactions.Transaction;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author bsneade
 */
public class SpreedlyPaymentMethodServiceIntegrationTest {
    private final SpreedlyPaymentMethodService spreedlyPaymentMethodService = new SpreedlyPaymentMethodService();
    private final ObjectFactory objectFactory = new ObjectFactory();
    
    @BeforeClass
    public void beforeClass() {
        //configure the service
        spreedlyPaymentMethodService.setEnvironmentKey("KPAdXK2qCu0NClgNmvfT45Q0jIu");
        spreedlyPaymentMethodService.setAccessSecret("W3NcbvqpLFNv3ZV9m3Cbj9id4ygkRmQtjFc9J22tsYhnzxkKqjuEqINXBnAijpud");
        
        final Client client = ClientBuilder.newClient()
                .register(new LoggingInterceptor());
        
        spreedlyPaymentMethodService.setClient(client);
    }
    
    @Test
    public void testAdd() {
        //set up data
        final PaymentMethod paymentMethod = objectFactory.createPaymentMethod();
        paymentMethod.setEmail("jpicard@ufp.net");
        final CreditCard creditCard = objectFactory.createCreditCard();
        creditCard.setFirstName("Jean-Luc");
        creditCard.setLastName("Picard");
        creditCard.setNumber("5555555555554444");
        creditCard.setVerificationValue("423");
        creditCard.setMonth("3");
        creditCard.setYear("2032");
        paymentMethod.setCreditCard(creditCard);
//        paymentMethod.setData("<paymentMethodId>448</paymentMethodId>");
        paymentMethod.setRetained(Boolean.TRUE);

        //call method under test
        final Transaction transaction = spreedlyPaymentMethodService.add(paymentMethod);

        //assertions
    }
}
