package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author bsneade
 */
public class SpreedlyTransactionServiceIntegrationTest {
    
     private final SpreedlyTransactionService spreedlyTransactionService = new SpreedlyTransactionService();
    private final ObjectFactory objectFactory = new ObjectFactory();
    
    @BeforeClass
    public void beforeClass() {
        //configure the service
        spreedlyTransactionService.setEnvironmentKey("KPAdXK2qCu0NClgNmvfT45Q0jIu");
        spreedlyTransactionService.setAccessSecret("W3NcbvqpLFNv3ZV9m3Cbj9id4ygkRmQtjFc9J22tsYhnzxkKqjuEqINXBnAijpud");
        
        final Client client = ClientBuilder.newClient()
                .register(new LoggingInterceptor());
        
        spreedlyTransactionService.setClient(client);
    }
    
    @Test
    public void testPurchase() {
        final Transaction transaction = new Transaction();
        transaction.setAmount(1); //one cent
        transaction.setCurrencyCode("USD");
        transaction.setPaymentMethodToken("HSlvaXfid2rdAGxdqVerre7kU7");
        
//        final Transaction returnTransaction = spreedlyTransactionService.purchase(transaction);
    }
}
