package com.niyamit.spreedly;

import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import com.niyamit.spreedly.transactions.Transaction;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.mockito.Mockito.*;

/**
 *
 * @author bsneade
 */
public class SpreedlyInvokerTest {
    
    @Mock
    private Client client;
    
    @BeforeMethod
    public void setUpMethod() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Test of invoke method, of class SpreedlyInvoker.
     */
    @Test
    public void testInvoke() {
        //set up test data
        final String environmentKey = "environmentKey";
        final String accessSecret = "accessSecret";
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        final String targetUrl = "http://target.url";
        final String method = "POST";
        final Transaction transaction = new Transaction();
        final PaymentMethod returnPaymentMethod = new PaymentMethod();
        
        //set up mocks
        final WebTarget webTarget = mock(WebTarget.class);
        when(client.target(targetUrl)).thenReturn(webTarget);
        final Invocation.Builder builder = mock(Invocation.Builder.class);
        when(webTarget.request()).thenReturn(builder);
        when(builder.header(anyString(), any())).thenReturn(builder);
        final Response response = mock(Response.class);
        when(builder.method(eq(method), any(Entity.class))).thenReturn(response);
        when(response.getStatus()).thenReturn(200); //success response
        when(response.readEntity(PaymentMethod.class)).thenReturn(returnPaymentMethod);
        
        //method under test
        final PaymentMethod responsePaymentMethod = spreedlyInvoker.invoke(targetUrl, method, transaction, PaymentMethod.class);
        
        //assertions 
        assertNotNull(responsePaymentMethod);
        assertEquals(returnPaymentMethod, responsePaymentMethod);
        
        //verifications
        verify(client, times(1)).target(targetUrl);
        verify(webTarget, times(1)).request();
        verify(builder, times(1)).method(eq(method), any(Entity.class));
        verify(response, times(1)).readEntity(PaymentMethod.class);
    }
    
}
