package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author bsneade
 */
public class QuickStartTest {
    
    private JAXBContext jaxbContext;
    private Marshaller marshaller;
    private ObjectFactory objectFactory = new ObjectFactory();
    
    @BeforeClass
    public void beforeClass() throws JAXBException {
        jaxbContext = JAXBContext.newInstance("com.niyamit.spreedly");
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
    }
    
     @Test
    public void testAddPaymentGateway_request() throws JAXBException, IOException {
        //set up data
        final Gateway gateway = objectFactory.createGateway();
        gateway.setGatewayType("test");

        //call method under test
        final StringWriter stringWriter = new StringWriter();
        marshaller.marshal(gateway, stringWriter);
        
        //assertions
        System.out.println(stringWriter.toString());
    }
    
    @Test
    public void testAddPaymentGateway_response() throws JAXBException, IOException {
        //set up data
        final Gateway gateway = objectFactory.createGateway();
        gateway.setToken("RZmqAxsok9haWD7nB2VlxwuqvLs");
        gateway.setGatewayType("test");
        gateway.setName("Spreedly Test");
        final GatewayClass gatewayClass = new GatewayClass();
        gatewayClass.setSupportsPurchase(Boolean.TRUE);
        gatewayClass.setSupportsAuthorize(Boolean.TRUE);
        gatewayClass.setSupportsCapture(Boolean.TRUE);
        gatewayClass.setSupportsCredit(Boolean.TRUE);
        gatewayClass.setSupportsVoid(Boolean.TRUE);
        gatewayClass.setSupportsReferencePurchase(Boolean.TRUE);
        gatewayClass.setSupportsPurchaseViaPreauthorization(Boolean.TRUE);
        gatewayClass.setSupportsPurchase(Boolean.TRUE);
        gatewayClass.setSupports3dsecurePurchase(Boolean.TRUE);
        gatewayClass.setSupports3dsecureAuthorize(Boolean.TRUE);
        gatewayClass.setSupportsStore(Boolean.TRUE);
        gatewayClass.setSupportsRemove(Boolean.TRUE);
        gateway.setCharacteristics(gatewayClass);
        
        //call method under test
        final StringWriter stringWriter = new StringWriter();
        marshaller.marshal(gateway, stringWriter);
        
        //assertions
        System.out.println(stringWriter.toString());
    }
    
    
   @Test
   public void testProcessPayment_request() {
//       final Transaction transaction = objectFactory.createTransaction();
//       transaction.set
//       
//       //call method under test
//        final StringWriter stringWriter = new StringWriter();
//        marshaller.marshal(transaction, stringWriter);
//        
//        //assertions
//        System.out.println(stringWriter.toString());
   }
    
}
