package com.niyamit.spreedly;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author bsneade
 */
@XmlRootElement(name = "gateway")
public class Gateway extends Model {

    
    private String gatewayType;
    private String state;
    private String name;
    private List<Credential> credentials;
    private GatewayClass characteristics;
    private String login;
    private String password;

    @XmlElement(name="gateway_type")
    public String getGatewayType() {
        return gatewayType;
    }

    public void setGatewayType(String gatewayType) {
        this.gatewayType = gatewayType;
    }

    @XmlElement(name="state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @XmlElement(name="name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<Credential> credentials) {
        this.credentials = credentials;
    }

    @XmlElement(name="characteristics")
    public GatewayClass getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(GatewayClass characteristics) {
        this.characteristics = characteristics;
    }

    @XmlElement(name="login")
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @XmlElement(name="password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
