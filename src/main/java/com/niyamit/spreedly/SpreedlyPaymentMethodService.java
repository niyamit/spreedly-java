package com.niyamit.spreedly;

import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import com.niyamit.spreedly.transactions.Transaction;
import java.net.URLEncoder;
import javax.ws.rs.client.Client;

/**
 *
 * @author bsneade
 */
public class SpreedlyPaymentMethodService implements PaymentMethodService {
    
    private String environmentKey;
    private String accessSecret;
    private Client client;

    public void setEnvironmentKey(String environmentKey) {
        this.environmentKey = environmentKey;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public void setClient(Client client) {
        this.client = client;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction add(final PaymentMethod paymentMethod) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods.xml", "POST", paymentMethod, Transaction.class);
    }
    
    @Override
    public Transaction retain(final PaymentMethod paymentMethod) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods/" + paymentMethod.getToken() + "/retain.xml", "PUT", paymentMethod, Transaction.class);
    }
    
    @Override
    public Transaction redact(final PaymentMethod paymentMethod) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods/" + paymentMethod.getToken() + "/redact.xml", "PUT", paymentMethod, Transaction.class);
    }
    
    @Override
    public PaymentMethod get(final String token) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods/" + URLEncoder.encode(token) + ".xml", "GET", null, PaymentMethod.class);
    }
    
    @Override
    public PaymentMethod update(final PaymentMethod paymentMethod) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods/" + paymentMethod.getToken() + ".xml", "PUT", paymentMethod, PaymentMethod.class);
    }
    
    @Override
    public Transaction recache(final PaymentMethod paymentMethod) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/payment_methods/" + paymentMethod.getToken() + "/recache.xml", "PUT", paymentMethod, Transaction.class);
    }
}
