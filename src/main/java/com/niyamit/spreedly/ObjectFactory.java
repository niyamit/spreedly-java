package com.niyamit.spreedly;

import com.niyamit.spreedly.paymentmethods.CreditCard;
import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import com.niyamit.spreedly.transactions.Transaction;
import javax.xml.bind.annotation.XmlRegistry;

/**
 *
 * @author bsneade
 */
@XmlRegistry
public class ObjectFactory {
    
    public Gateways createGateways() {
        return new Gateways();
    }
 
    public Gateway createGateway() {
        return new Gateway();
    }

    public Transaction createTransaction() {
        return new Transaction();
    }

    public PaymentMethod createPaymentMethod() {
        return new PaymentMethod();
    }

    public CreditCard createCreditCard() {
        return new CreditCard();
    }
    
}
