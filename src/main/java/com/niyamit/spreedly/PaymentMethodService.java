package com.niyamit.spreedly;

import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import com.niyamit.spreedly.transactions.Transaction;

/**
 *
 * @author bsneade
 */
public interface PaymentMethodService {

    Transaction add(PaymentMethod paymentMethod);

    PaymentMethod get(String token);

    Transaction recache(PaymentMethod paymentMethod);

    Transaction redact(PaymentMethod paymentMethod);

    Transaction retain(PaymentMethod paymentMethod);

    PaymentMethod update(PaymentMethod paymentMethod);
    
}
