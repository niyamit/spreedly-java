package com.niyamit.spreedly;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class Credential implements Serializable {

    
    private String name;
    
    private String label;
    
    private Boolean safe;

    public String getName() {
        return name;
    }

    @XmlElement(name="name")
    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    @XmlElement(name="label")
    public void setLabel(String label) {
        this.label = label;
    }

    @XmlElement(name="safe")
    public Boolean isSafe() {
        return safe;
    }

    public void setSafe(Boolean safe) {
        this.safe = safe;
    }
    
}
