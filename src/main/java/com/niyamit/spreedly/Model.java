package com.niyamit.spreedly;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import org.joda.time.DateTime;

/**
 *
 * @author bsneade
 */
public class Model implements Serializable {
    
    private String token;

    private DateTime createdAt;
    
    private DateTime updatedAt;

    @XmlElement(name = "token")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @XmlElement(name = "created_at")
    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    @XmlElement(name = "updated_at")
    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    
}
