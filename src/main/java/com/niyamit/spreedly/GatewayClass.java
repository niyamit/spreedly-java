package com.niyamit.spreedly;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class GatewayClass implements Serializable {

    private String gatewayType;

    private String name;

    private String homepage;

    private String companyName;

    private Boolean supportsPurchase;

    private Boolean supportsAuthorize;

    private Boolean supportsCapture;

    private Boolean supportsCredit;

    private Boolean supportsVoid;

    private Boolean supportsReferencePurchase;

    private Boolean supportsPurchaseViaPreauthorization;

    private Boolean supportsOffsitePurchase;

    private Boolean supportsOffsiteAuthorize;

    private Boolean supports3dsecurePurchase;

    private Boolean supports3dsecureAuthorize;

    private Boolean supportsStore;

    private Boolean supportsRemove;

    @XmlElement(name = "gateway_type")
    public String getGatewayType() {
        return gatewayType;
    }

    public void setGatewayType(String gatewayType) {
        this.gatewayType = gatewayType;
    }

    @XmlElement(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "homepage")
    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @XmlElement(name = "company_name")
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @XmlElement(name = "supports_purchase")
    public Boolean isSupportsPurchase() {
        return supportsPurchase;
    }

    public void setSupportsPurchase(Boolean supportsPurchase) {
        this.supportsPurchase = supportsPurchase;
    }

    @XmlElement(name = "supports_authorize")
    public Boolean isSupportsAuthorize() {
        return supportsAuthorize;
    }

    public void setSupportsAuthorize(Boolean supportsAuthorize) {
        this.supportsAuthorize = supportsAuthorize;
    }

    @XmlElement(name = "supports_capture")
    public Boolean isSupportsCapture() {
        return supportsCapture;
    }

    public void setSupportsCapture(Boolean supportsCapture) {
        this.supportsCapture = supportsCapture;
    }

    @XmlElement(name = "supports_credit")
    public Boolean isSupportsCredit() {
        return supportsCredit;
    }

    public void setSupportsCredit(Boolean supportsCredit) {
        this.supportsCredit = supportsCredit;
    }

    @XmlElement(name = "supports_void")
    public Boolean isSupportsVoid() {
        return supportsVoid;
    }

    public void setSupportsVoid(Boolean supportsVoid) {
        this.supportsVoid = supportsVoid;
    }

    @XmlElement(name = "supports_reference_purchase")
    public Boolean isSupportsReferencePurchase() {
        return supportsReferencePurchase;
    }

    public void setSupportsReferencePurchase(Boolean supportsReferencePurchase) {
        this.supportsReferencePurchase = supportsReferencePurchase;
    }

    @XmlElement(name = "supports_purchase_via_preauthorization")
    public Boolean isSupportsPurchaseViaPreauthorization() {
        return supportsPurchaseViaPreauthorization;
    }

    public void setSupportsPurchaseViaPreauthorization(Boolean supportsPurchaseViaPreauthorization) {
        this.supportsPurchaseViaPreauthorization = supportsPurchaseViaPreauthorization;
    }

    @XmlElement(name = "supports_offsite_purchase")
    public Boolean isSupportsOffsitePurchase() {
        return supportsOffsitePurchase;
    }

    public void setSupportsOffsitePurchase(Boolean supportsOffsitePurchase) {
        this.supportsOffsitePurchase = supportsOffsitePurchase;
    }

    @XmlElement(name = "supports_offsite_authorize")
    public Boolean isSupportsOffsiteAuthorize() {
        return supportsOffsiteAuthorize;
    }

    public void setSupportsOffsiteAuthorize(Boolean supportsOffsiteAuthorize) {
        this.supportsOffsiteAuthorize = supportsOffsiteAuthorize;
    }

    @XmlElement(name = "supports_3dsecure_purchase")
    public Boolean isSupports3dsecurePurchase() {
        return supports3dsecurePurchase;
    }

    public void setSupports3dsecurePurchase(Boolean supports3dsecurePurchase) {
        this.supports3dsecurePurchase = supports3dsecurePurchase;
    }

    @XmlElement(name = "supports_3dsecure_authorize")
    public Boolean isSupports3dsecureAuthorize() {
        return supports3dsecureAuthorize;
    }

    public void setSupports3dsecureAuthorize(Boolean supports3dsecureAuthorize) {
        this.supports3dsecureAuthorize = supports3dsecureAuthorize;
    }

    @XmlElement(name = "supports_store")
    public Boolean isSupportsStore() {
        return supportsStore;
    }

    public void setSupportsStore(Boolean supportsStore) {
        this.supportsStore = supportsStore;
    }

    @XmlElement(name = "supports_remove")
    public Boolean isSupportsRemove() {
        return supportsRemove;
    }

    public void setSupportsRemove(Boolean supportsRemove) {
        this.supportsRemove = supportsRemove;
    }
}
