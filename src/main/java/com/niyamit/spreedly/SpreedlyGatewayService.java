package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;
import javax.ws.rs.client.Client;

/**
 *
 * @author bsneade
 */
public class SpreedlyGatewayService implements GatewayService {

    private String environmentKey;
    private String accessSecret;
    private Client client;

    public void setEnvironmentKey(String environmentKey) {
        this.environmentKey = environmentKey;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Gateway add(final Gateway gateway) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways.xml", "POST", gateway, Gateway.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction retain(final Gateway gateway) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + gateway.getToken() + "/retain.xml", "PUT", gateway, Transaction.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Transaction redact(final Gateway gateway) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + gateway.getToken() + "/redact.xml", "PUT", gateway, Transaction.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Gateway update(final Gateway gateway) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + gateway.getToken() + ".xml", "PUT", gateway, Gateway.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Gateways list() {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways.xml", "GET", null, Gateways.class);
    }

    @Override
    public Gateway get(String token) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + token + ".xml", "GET", null, Gateway.class);
    }

}
