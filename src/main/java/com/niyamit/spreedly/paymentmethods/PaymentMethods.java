package com.niyamit.spreedly.paymentmethods;

import com.niyamit.spreedly.*;
import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bsneade
 */
@XmlRootElement(name = "payment_methods")
public class PaymentMethods implements Serializable {
    
    private List<PaymentMethod> paymentMethods;

    @XmlElement(name="payment_method")
    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }
    
}
