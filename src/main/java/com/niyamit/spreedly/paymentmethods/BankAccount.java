package com.niyamit.spreedly.paymentmethods;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class BankAccount extends PaymentMethod {

    @XmlElement(name = "first_name")
    private String firstName;
    @XmlElement(name = "last_name")
    private String lastName;
    @XmlElement(name = "full_name")
    private String fullName;
    @XmlElement(name = "bank_name")
    private String bankName;
    @XmlElement(name = "account_type")
    private String accountType;
    @XmlElement(name = "account_holder_type")
    private String accountHolderType;
    @XmlElement(name = "routing_number_display_digits")
    private String routingNumberDisplayDigits;
    @XmlElement(name = "account_number_display_digits")
    private String accountNumberDisplayDigits;
    @XmlElement(name = "routing_number")
    private String routingNumber;
    @XmlElement(name = "account_number")
    private String accountNumber;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountHolderType() {
        return accountHolderType;
    }

    public void setAccountHolderType(String accountHolderType) {
        this.accountHolderType = accountHolderType;
    }

    public String getRoutingNumberDisplayDigits() {
        return routingNumberDisplayDigits;
    }

    public void setRoutingNumberDisplayDigits(String routingNumberDisplayDigits) {
        this.routingNumberDisplayDigits = routingNumberDisplayDigits;
    }

    public String getAccountNumberDisplayDigits() {
        return accountNumberDisplayDigits;
    }

    public void setAccountNumberDisplayDigits(String accountNumberDisplayDigits) {
        this.accountNumberDisplayDigits = accountNumberDisplayDigits;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    
}
