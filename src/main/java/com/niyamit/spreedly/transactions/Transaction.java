package com.niyamit.spreedly.transactions;

import com.niyamit.spreedly.Model;
import com.niyamit.spreedly.paymentmethods.CreditCard;
import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bsneade
 */
@XmlRootElement(name = "transaction")
public class Transaction extends Model {
    
    private String state;

    private String message;

    private Boolean succeeded;

    private String currencyCode;

    private Integer amount;

    private String email;

    private String storageState;

    private String data;

    private String paymentMethodToken;

    private CreditCard creditCard;
    
    private String transactionType;
    
    private String orderId;
    
    private String ip;
    
    private String description;
    
    private String merchantNameDescriptor;
    
    private String merchantLocationDescriptor;
    
    private String gatewaySpecificFields;
    
    private String gatewaySpecificResponseFields;
    
    private String gatewayTransactionId;

    private String gatewayToken;
    
    private Response response;
    
    private PaymentMethod paymentMethod;
    
    private Boolean onTestGateway;
    
    

    @XmlElement(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @XmlElement(name = "succeeded")
    public Boolean getSucceeded() {
        return succeeded;
    }

    public void setSucceeded(Boolean succeeded) {
        this.succeeded = succeeded;
    }

    @XmlElement(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlElement(name = "storage_state")
    public String getStorageState() {
        return storageState;
    }

    public void setStorageState(String storageState) {
        this.storageState = storageState;
    }

    @XmlElement(name = "data")
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @XmlElement(name = "credit_card")
    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    @XmlElement(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @XmlElement(name = "amount")
    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @XmlElement(name = "payment_method_token")
    public String getPaymentMethodToken() {
        return paymentMethodToken;
    }

    public void setPaymentMethodToken(String paymentMethodToken) {
        this.paymentMethodToken = paymentMethodToken;
    }

    @XmlElement(name = "transaction_type")
    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getOrderId() {
        return orderId;
    }

    @XmlElement(name = "orider_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIp() {
        return ip;
    }

    @XmlElement(name = "ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement(name = "description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getMerchantNameDescriptor() {
        return merchantNameDescriptor;
    }

    @XmlElement(name = "merchant_name_descriptor")
    public void setMerchantNameDescriptor(String merchantNameDescriptor) {
        this.merchantNameDescriptor = merchantNameDescriptor;
    }

    public String getMerchantLocationDescriptor() {
        return merchantLocationDescriptor;
    }

    @XmlElement(name = "merchant_location_descriptor")
    public void setMerchantLocationDescriptor(String merchantLocationDescriptor) {
        this.merchantLocationDescriptor = merchantLocationDescriptor;
    }

    public String getGatewaySpecificFields() {
        return gatewaySpecificFields;
    }

    @XmlElement(name = "gateway_specific_fields")
    public void setGatewaySpecificFields(String gatewaySpecificFields) {
        this.gatewaySpecificFields = gatewaySpecificFields;
    }

    public String getGatewaySpecificResponseFields() {
        return gatewaySpecificResponseFields;
    }

    @XmlElement(name = "gateway_sepcific_response_fields")
    public void setGatewaySpecificResponseFields(String gatewaySpecificResponseFields) {
        this.gatewaySpecificResponseFields = gatewaySpecificResponseFields;
    }

    public String getGatewayTransactionId() {
        return gatewayTransactionId;
    }

    @XmlElement(name = "gateway_transaction_id")
    public void setGatewayTransactionId(String gatewayTransactionId) {
        this.gatewayTransactionId = gatewayTransactionId;
    }

    @XmlElement(name = "gateway_token")
    public String getGatewayToken() {
        return gatewayToken;
    }

    public void setGatewayToken(String gatewayToken) {
        this.gatewayToken = gatewayToken;
    }

    @XmlElement(name = "response")
    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @XmlElement(name = "payment_method")
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
