package com.niyamit.spreedly.transactions;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class Capture {

    @XmlElement(name = "currency_code")
    private String currencyCode;

    @XmlElement(name = "reference_token")
    private String referenceToken;

    @XmlElement(name = "amount")
    private Double amount;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getReferenceToken() {
        return referenceToken;
    }

    public void setReferenceToken(String referenceToken) {
        this.referenceToken = referenceToken;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
