package com.niyamit.spreedly.transactions;

import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class RetainPaymentMethod extends Transaction {
    
    @XmlElement(name="payment_method")
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
}
