package com.niyamit.spreedly.transactions;

import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class AddPaymentMethod extends Transaction {
    
    @XmlElement(name="retained") 
    private Boolean retained;
    
    @XmlElement(name="payment_method")
    private PaymentMethod paymentMethod;

    public Boolean isRetained() {
        return retained;
    }

    public void setRetained(Boolean retained) {
        this.retained = retained;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
}
