package com.niyamit.spreedly.transactions;

import com.niyamit.spreedly.Gateway;
import com.niyamit.spreedly.paymentmethods.PaymentMethod;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class RedactGateway extends Transaction {
    
    @XmlElement(name="gateway")
    private Gateway gateway;
    
    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }
    
}
