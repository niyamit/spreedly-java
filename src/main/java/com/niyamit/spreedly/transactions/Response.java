package com.niyamit.spreedly.transactions;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlElement;
import org.joda.time.DateTime;

/**
 *
 * @author bsneade
 */
public class Response implements Serializable {

    private Boolean success;

    private String message;

    private String avsCode;

    private String avsMessage;

    private String cvvCode;

    private String cvvMessage;

    private Boolean pending;

    private String errorCode;

    private String errorDetail;

    private Boolean cancelled;

    private DateTime createdAt;

    private DateTime updatedAt;

    @XmlElement(name = "success")
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @XmlElement(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @XmlElement(name = "avs_code")
    public String getAvsCode() {
        return avsCode;
    }

    public void setAvsCode(String avsCode) {
        this.avsCode = avsCode;
    }

    @XmlElement(name = "avs_message")
    public String getAvsMessage() {
        return avsMessage;
    }

    public void setAvsMessage(String avsMessage) {
        this.avsMessage = avsMessage;
    }

    @XmlElement(name = "cvv_code")
    public String getCvvCode() {
        return cvvCode;
    }

    public void setCvvCode(String cvvCode) {
        this.cvvCode = cvvCode;
    }

    @XmlElement(name = "cvv_message")
    public String getCvvMessage() {
        return cvvMessage;
    }

    public void setCvvMessage(String cvvMessage) {
        this.cvvMessage = cvvMessage;
    }

    @XmlElement(name = "pending")
    public Boolean getPending() {
        return pending;
    }

    public void setPending(Boolean pending) {
        this.pending = pending;
    }

    @XmlElement(name = "error_code")
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @XmlElement(name = "error_detail")
    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }

    @XmlElement(name = "cancelled")
    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    @XmlElement(name = "created_at")
    public DateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    @XmlElement(name = "updated_at")
    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

}
