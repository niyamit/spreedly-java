package com.niyamit.spreedly.transactions;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class Refund extends GatewayTransaction {

    @XmlElement(name = "reference_token")
    private String referenceToken;

    public String getReferenceToken() {
        return referenceToken;
    }

    public void setReferenceToken(String referenceToken) {
        this.referenceToken = referenceToken;
    }

}
