package com.niyamit.spreedly.transactions;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class GatewayTransaction extends Transaction {

    @XmlElement(name = "order_id")
    private String orderId;
    @XmlElement(name = "ip")
    private String ip;
    @XmlElement(name = "description")
    private String description;
    @XmlElement(name = "gateway_token")
    private String gatewayToken;
    @XmlElement(name = "gateway_transaction_id")
    private String gatewayTransactionId;
    @XmlElement(name = "merchant_name_descriptor")
    private String merchantNameDescriptor;
    @XmlElement(name = "merchant_location_descriptor")
    private String merchantLocationDescriptor;

    @XmlElement(name = "on_test_gateway")
    private Boolean onTestGateway;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGatewayToken() {
        return gatewayToken;
    }

    public void setGatewayToken(String gatewayToken) {
        this.gatewayToken = gatewayToken;
    }

    public String getGatewayTransactionId() {
        return gatewayTransactionId;
    }

    public void setGatewayTransactionId(String gatewayTransactionId) {
        this.gatewayTransactionId = gatewayTransactionId;
    }

    public String getMerchantNameDescriptor() {
        return merchantNameDescriptor;
    }

    public void setMerchantNameDescriptor(String merchantNameDescriptor) {
        this.merchantNameDescriptor = merchantNameDescriptor;
    }

    public String getMerchantLocationDescriptor() {
        return merchantLocationDescriptor;
    }

    public void setMerchantLocationDescriptor(String merchantLocationDescriptor) {
        this.merchantLocationDescriptor = merchantLocationDescriptor;
    }

    public Boolean isOnTestGateway() {
        return onTestGateway;
    }

    public void setOnTestGateway(Boolean onTestGateway) {
        this.onTestGateway = onTestGateway;
    }
    
    
}
