package com.niyamit.spreedly;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author bsneade
 */
public class AuthMode implements Serializable {
    
    @XmlElement(name="auth_mode_type")
    private String authModeType;
    @XmlElement(name="name")
    private String name;
    private List<Credential> credentials;

    public String getAuthModeType() {
        return authModeType;
    }

    public void setAuthModeType(String authModeType) {
        this.authModeType = authModeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Credential> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<Credential> credentials) {
        this.credentials = credentials;
    }
    
}
