package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;
import javax.ws.rs.client.Client;

/**
 *
 * @author bsneade
 */
public class SpreedlyTransactionService implements TransactionService {

    private String environmentKey;
    private String accessSecret;
    private Client client;

    public void setEnvironmentKey(String environmentKey) {
        this.environmentKey = environmentKey;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public Transaction purchase(final Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + transaction.getToken() + "/purchase.xml", "POST", transaction, Transaction.class);
    }

    @Override
    public Transaction authorize(final Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + transaction.getToken() + "/authorize.xml", "POST", transaction, Transaction.class);
    }

    @Override
    public Transaction capture(final Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/transactions/" + transaction.getToken() + "/capture.xml", "POST", transaction, Transaction.class);
    }

    @Override
    public Transaction voidTransaction(final Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/transactions/" + transaction.getToken() + "/void.xml", "POST", transaction, Transaction.class);
    }

    @Override
    public Transaction credit(final Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/transactions/" + transaction.getToken() + "/credit.xml", "POST", transaction, Transaction.class);
    }

	@Override
	public Transaction verify(Transaction transaction) {
        final SpreedlyInvoker spreedlyInvoker = new SpreedlyInvoker(environmentKey, accessSecret, client);
        return spreedlyInvoker.invoke("https://core.spreedly.com/v1/gateways/" + transaction.getToken() + "/verify.xml", "POST", transaction, Transaction.class);
	}

}
