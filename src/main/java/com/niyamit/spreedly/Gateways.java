package com.niyamit.spreedly;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bsneade
 */
@XmlRootElement(name = "gateways")
public class Gateways implements Serializable {
    
    private List<Gateway> gateways;

    @XmlElement(name="gateway")
    public List<Gateway> getGateways() {
        return gateways;
    }

    public void setGateways(List<Gateway> gateways) {
        this.gateways = gateways;
    }
    
}
