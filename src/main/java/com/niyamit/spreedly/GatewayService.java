package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;

/**
 *
 * @author bsneade
 */
public interface GatewayService {

    /**
     * @see <a href="https://docs.spreedly.com/gateways/adding/">Adding a Gateway</a>
     * @param gateway to add.
     * @return The gateway details.
     */
    Gateway add(Gateway gateway);
    
    /**
     * @see <a href="https://docs.spreedly.com/gateways/getting/">Getting Gateways</a>
     * @return A container with a set of {@link Gateway}s.
     */
    Gateways list();

    /**
     * @see <a href="https://docs.spreedly.com/gateways/redacting/">Redacting a Gateway</a>
     * @param gateway to redact.
     * @return The gateway details.
     */
    Transaction redact(Gateway gateway);

    /**
     * @see <a href="https://docs.spreedly.com/gateways/retaining/">Retaining a Gateway</a>
     * @param gateway to retain.
     * @return The transaction details.
     */
    Transaction retain(Gateway gateway);

    /**
     * @see <a href="https://docs.spreedly.com/gateways/updating/">Updating a Gateway</a>
     * @param gateway to update.
     * @return The gateway details.
     */
    Gateway update(Gateway gateway);
    
    /**
     * @see <a href="https://docs.spreedly.com/gateways/getting/#getting-one-gateway">Getting One Gateway</a>
     * @param token for the gateway
     * @return the gateway corresponding to the token
     */
    Gateway get(String token);
}
