package com.niyamit.spreedly;

import com.niyamit.spreedly.transactions.Transaction;

/**
 *
 * @author bsneade
 */
public interface TransactionService {

    Transaction authorize(final Transaction transaction);

    Transaction capture(final Transaction transaction);

    Transaction credit(final Transaction transaction);

    Transaction purchase(final Transaction transaction);

    Transaction voidTransaction(final Transaction transaction);
    
    Transaction verify(final Transaction transaction);
}
